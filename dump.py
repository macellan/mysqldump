#!/usr/bin/env python
#-*-coding:utf-8-*-

import os
import ConfigParser
import time
import getpass
import tarfile
import sys

help = """usage:
python dump.py conf_file
"""

config = ConfigParser.RawConfigParser()

try:
    config.read('%s.cfg'%sys.argv[1])
    dbname = config.get('System', 'dbname')
    dbusername = config.get('System', 'dbusername')
    username = config.get('System', 'username')
    hostname = config.get('System', 'hostname')
    localdbname = config.get('System', 'localdbname')
    localdbusername = config.get('System', 'localdbusername')
    remotedumpdir = config.get('System', 'remotedumpdir')
    
    passwd = getpass.getpass('Password: ')
    
    curtime = time.strftime("%Y.%m.%d.%H.%M.%S", time.gmtime())
    filename = "%s-%s-%s"%(hostname, dbname, curtime)
    print "filename is = ", filename
    
    command = """ssh %s@%s 'cd backups;
    mysqldump -u %s -p%s %s>%s.sql;
    tar -czvf %s.sql.tar.gz %s.sql;'"""%(username, hostname, dbusername, passwd, dbname, filename, filename, filename)
    
    command2 = """scp %s@%s:~/%s/%s.sql.tar.gz ."""%(username, hostname, remotedumpdir, filename)
    
    try:
        print "Password for dumping db: "
        os.system(command)
        print "File created. Downloading..."
        
        time.sleep(5)
        
        print "Sleeping 5 seconds"
        os.system(command2)
        
        print "File Downloaded. Extracting..."
        tar = tarfile.open("%s.sql.tar.gz"%filename)
        tar.extractall()
        tar.close()
        print "All Done!"
        print "You can run mysql -u %s -p %s<%s.sql"%(localdbusername, localdbname, filename)
        
    except Exception as e:
        print e
except:
    print help